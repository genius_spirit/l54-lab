export default class WinCalc {

  constructor(cards){
    this.cards = cards;
    this.suits = this.cards.map(card => card.suit);
    this.ranks = this.cards.map(card => card.rank);

    this.isFlush = this.suits.every(suit => suit === this.suits[0]);
  }

  getDuplicateRanksNumbers() {
    this.ranksNumber = {};
    this.ranks.forEach(rank => {
      if (!this.ranksNumber[rank]) {
        this.ranksNumber[rank] = 1;
      } else {
        this.ranksNumber[rank]++;
      }
    });
    return this.ranksNumber;
  }

  isRoyalFlush() {
    return this.isFlush &&
      this.ranks.includes('10') &&
      this.ranks.includes('J') &&
      this.ranks.includes('Q') &&
      this.ranks.includes('K') &&
      this.ranks.includes('A')
  }

  isFourOfAKind() {
    const four = this.getDuplicateRanksNumbers();
    return Object.values(four).includes(4);
  }

  isFullHouse() {
    const fullHouse = this.getDuplicateRanksNumbers();
    return (Object.values(fullHouse).includes(3)
      && Object.values(fullHouse).includes(2));
  }

  isThreeOfAKind() {
    const tripleNumbers = this.getDuplicateRanksNumbers();
    return Object.values(tripleNumbers).includes(3)
  }

  isTwoPair() {
    const pairNumbers = this.getDuplicateRanksNumbers();
    const numbers = Object.values(pairNumbers); // [2, 2, 1]
    let count = null;
    numbers.forEach(number => {
      if (number > 1) {
        count++;
      }
    });
    if (count > 1) return true;
  }

  isPair() {
    const doubleNumbers = this.getDuplicateRanksNumbers();
    return Object.values(doubleNumbers).includes(2);
  }

  resultsOutcome() {
    if (this.isRoyalFlush()) {
      return 'Royal Flush';
    } else if (this.isFlush) {
      return 'Flush';
    } else if (this.isFourOfAKind()) {
      return 'Four of a kind';
    } else if (this.isFullHouse()) {
      return 'Full house';
    } else if (this.isThreeOfAKind()) {
      return 'Three of a kind';
    } else if (this.isTwoPair()) {
      return 'Two Pair';
    } else if (this.isPair()) {
      return 'A Pair';
    } else {
        return 'Nothing';
    }
  }
}