import React, { Component } from 'react';
import './App.css';
import Card from "./Card/Card";
import WinCalc from './WinCalc';

class App extends Component {

  ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
  suits = ['S', 'C', 'H', 'D'];

  state = {
    random: [],
    outcome: ''
  };

  randomCards = () => {
    let array = [];
    let counter = 5;
    while (counter > 0) {
      let random = Math.floor(Math.random() * this.ranks.length);
      let random2 = Math.floor(Math.random() * this.suits.length);
      let card = {rank: this.ranks[random], suit: this.suits[random2]};
      if (array.findIndex((item) => item.rank === card.rank && item.suit === card.suit) === -1 ) {
        array.push(card);
        counter--;
      }
    }
    console.log(array);
    const calc = new WinCalc(array);
    const outcome = calc.resultsOutcome();

    this.setState({random: array, outcome});
  };

  render() {
    return (
      <div>
        <button className='btn' onClick={this.randomCards}>Shuffle Cards</button>
        <div className="playingCards faceImages container">
          <ul className="table">
            {
              this.state.random.map((card, index) => {
              return (
                <li key={index}>
                  <Card suit={card.suit} rank={card.rank} />
                </li>)
              })
            }
          </ul>
          <div className="outcome">{this.state.outcome}</div>
        </div>
      </div>
    );
  }
}

export default App;
