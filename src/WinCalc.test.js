import WinCalc from "./WinCalc";

const royalFlush = [
  {suit: 'H', rank: '10'},
  {suit: 'H', rank: 'J'},
  {suit: 'H', rank: 'Q'},
  {suit: 'H', rank: 'K'},
  {suit: 'H', rank: 'A'}
];

const flush = [
  {suit: 'H', rank: '5'},
  {suit: 'H', rank: 'J'},
  {suit: 'H', rank: '10'},
  {suit: 'H', rank: '2'},
  {suit: 'H', rank: 'A'}
];

const four = [
  {suit: 'H', rank: '5'},
  {suit: 'D', rank: '5'},
  {suit: 'S', rank: '5'},
  {suit: 'C', rank: '5'},
  {suit: 'S', rank: 'A'}
];

const fullHouse = [
  {suit: 'H', rank: '5'},
  {suit: 'D', rank: '5'},
  {suit: 'S', rank: '5'},
  {suit: 'C', rank: 'A'},
  {suit: 'S', rank: 'A'}
];

const straight = [
  {suit: 'S', rank: '7'},
  {suit: 'D', rank: '8'},
  {suit: 'C', rank: '9'},
  {suit: 'H', rank: '10'},
  {suit: 'S', rank: 'A'}
];

const triple = [
  {suit: 'H', rank: '5'},
  {suit: 'D', rank: '5'},
  {suit: 'S', rank: 'A'},
  {suit: 'C', rank: '10'},
  {suit: 'S', rank: '5'}
];

const twoPair = [
  {suit: 'H', rank: '5'},
  {suit: 'D', rank: '5'},
  {suit: 'S', rank: 'A'},
  {suit: 'C', rank: '10'},
  {suit: 'H', rank: 'A'}
];

const pair = [
  {suit: 'D', rank: '8'},
  {suit: 'S', rank: 'J'},
  {suit: 'H', rank: '10'},
  {suit: 'D', rank: '5'},
  {suit: 'C', rank: '8'}
];


it('should be royalFlush', () => {
  const calc = new WinCalc(royalFlush);
  const result = calc.resultsOutcome();
  expect(result).toEqual('Royal Flush');
});

it('should be flush', () => {
  const calc = new WinCalc(flush);
  const result = calc.resultsOutcome();
  expect(result).toEqual('Flush');
});

it('should be four of a kind', () => {
  const calc = new WinCalc(four);
  const result = calc.resultsOutcome();
  expect(result).toEqual('Four of a kind');
});

it('should be full house', () => {
  const calc = new WinCalc(fullHouse);
  const result = calc.resultsOutcome();
  expect(result).toEqual('Full house');
});

it('should be three of a kind', () => {
  const calc = new WinCalc(triple);
  const result = calc.resultsOutcome();
  expect(result).toEqual('Three of a kind');
});

it('should be two pairs', () => {
  const calc = new WinCalc(twoPair);
  const result = calc.resultsOutcome();
  expect(result).toEqual('Two Pair');
});

it('should be a pair', () => {
  const calc = new WinCalc(pair);
  const result = calc.resultsOutcome();
  expect(result).toEqual('A Pair');
});

// it('should be straight', () => {
//   const calc = new WinCalc(straight);
//   const result = calc.resultsOutcome();
//   expect(result).toEqual('Straight');
// });
